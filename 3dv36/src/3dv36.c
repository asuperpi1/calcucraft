#include <string.h>
#include <fxcg/display.h>
#include <fxcg/keyboard.h>
#include <fxcg/misc.h>
#include <math.h>
#include <stdlib.h>
//#include <stdio.h>
/* -------Maintinance------
-how to add new cube types to map and hotbar.
-step one set defines at the defines for cubes in game (caz4f3) (control f to find this line)
-step two setup ensure that the type peramater in the nbt data of your block is unique. Possible behaviour if this is not implimented corerctly this may result in your block not being selected if the player destroys a block is the same type for the first time in the hotbar.
-step three add the block to the blocklist (dzf52)
-step four set the item to the hotbar and set a quantity.(lk78q)
-step five add block to the map array paste in your block objectdata name into the array.(qzo98)
-step six all done!

-how to change the map size
-step one change the map size values here (kjl78)
-step two change values at these locations (kvl873)
-step three increase the array size here (qpz167)
-step four done!
*/
//define usefull global values
int displaywidth=LCD_WIDTH_PX;
int displayheight=(LCD_HEIGHT_PX-24);
int halfdisplaywidth=LCD_WIDTH_PX/2;
int halfdisplayheight=LCD_HEIGHT_PX/2;

//unsigned short *vramstatic =0;
typedef struct point3d {//a point in 3 dimentions
   int x;
   int y;
   int z;
} point3d;

typedef struct quad {//structure for a collection of 4 points(3d) used in sides of a shape
   point3d a;
   point3d b;
   point3d c;
   point3d d;
} quad;
typedef struct cube {//structure for a collection of 8 points (3d) used for cubes
   point3d a;
   point3d b;
   point3d c;
   point3d d;
   point3d e;
   point3d f;
   point3d g;
   point3d h;
} cube;
typedef struct nbt{//The data about a cube such as how many breaking points left
	int type;
	unsigned short invisible;
	unsigned short invincible;
	unsigned short placeable;
	int breakingpoints;
}nbt;
typedef struct objectdata {//object data stores the sides of the cube colours and the logical nbt data about the cube
	unsigned short colours[6];
	nbt nbtdata;
} objectdata;
typedef struct hotbardata{
	objectdata object;
	int quantity;
	int stacklimit;
	unsigned short icon[25];
} hotbardata;
void Bdisp_LineC16();// this calls a system call for drawing a line.
__asm__(".text; .align 2; .global _Bdisp_LineC16; "
        "_Bdisp_LineC16: mov.l sc_addr, r2; mov.l 1f, r0; "
        "jmp @r2; nop; 1: .long 0x1d2; "
        "sc_addr: .long 0x80020070");
/*
void safepointvram( int x,int y, unsigned short colour){//draws a point if in display range
	unsigned short *writingpx=0;
	if (x>=0 && x<=displaywidth && y>=0 && y<=displayheight && (*vramstatic)!=0){
		writingpx=vramstatic+x+((y+24)*displaywidth);
		*writingpx=colour;
		//Bdisp_SetPoint_VRAM(x,y+24,colour);
	}
}
*/
void safepointvram( int x,int y, unsigned short colour){//draws a point if in display range
	if (x>=0 && x<=displaywidth && y>=0 && y<=displayheight){
		Bdisp_SetPoint_VRAM(x,y+24,colour);//draws the point IMPORTANT the 24 pixels are to make room for the status bar.
	}
	
}
void largepix(int x, int y, unsigned short colour){//draws pixels for the values in a 5*5 pixel size
	for (int ix=0; ix<5; ix=ix+1){
		for (int iy=0; iy<5; iy=iy+1){
			safepointvram(x+ix,y+iy,colour);
		}
	}
}
void drawlinehoriz(int y, int xa, int xb,unsigned short colour){//draws line horizontaly 
	int loop=0;
	if (xa<0){//check ranges so that they are within the display width
		xa=0;
	}
	if (xb>displaywidth){
		xb=displaywidth;
	}
	for(loop=xa;loop<=xb;loop=loop+1){//loop through each pixel and draw to display.
		safepointvram(loop,y,colour);
	}
}
void drawlinevert(int x,int ya,int yb, unsigned short colour){//draws a line vertically between the points
	int loop=0;
	if (ya<0){//check ranges so that they are within the display heightl
		ya=0;
	}
	if (yb>displayheight){
		yb=displayheight;
	}
	for(loop=ya;loop<=yb;loop=loop+1){//loop through each pixel and draw to display.
		safepointvram(x,loop,colour);
	}
}
int quadaverage(int za,int zb,int zc,int zd){// average 4 values (used to workout the order to render the sides.)
	int result =0;
	result=za+zb+zc+zd;
	result=result/4;
	return result;
}	
int drawfilled(int xa,int ya, int xb, int yb,int xc,int yc,int xd,int yd,unsigned short colour,int lowestvalue,int highestvalue){// draws filled quadralatrals.
	//defines for variables needed.
	int out=0;
    int outs[4];
    int xarray[4];
    int yarray[4];
    int i=0;
    int lengthiner=0;
    int temp=0;
	int lengthmc=0;
	float m=0;
    int c=0;
    int size=0;
	int carray[6];
	float marray[6];//floating point m array.
	//const int shift=262144; legacy optimisation
	//x build array
	//this uses lines defined by y=mx+c as constraints to work out intercepts
	xarray[0]=xa;
	xarray[1]=xb;
	xarray[2]=xc;
	xarray[3]=xd;
	//y build array
	yarray[0]=ya;
	yarray[1]=yb;
	yarray[2]=yc;
	yarray[3]=yd;
	for (i=0;i<4;i=i+1){//loop untill done
        xa=xarray[i];//load values into the recycled xa and ya and xb and yb
        ya=yarray[i];
        if ((i+1)>3){//if at end of the array use begining value to complete shape
            xb=xarray[0];
            yb=yarray[0];
        }
		else{
            xb=xarray[i+1];//otherwise use as normal
            yb=yarray[i+1];
        }
        m=0;//reset m and c
        c=0;
        if (!(xa==xb)){// if the values arent equal (prevents deviding by zero error)
            m=(float)(ya-yb)/(xa-xb);//use y=mx+c calulate m by working out gradiant.
            c=ya-(xa*m);
			marray[lengthmc]=m;
			carray[lengthmc]=c;
			lengthmc=lengthmc+1;
		}
		else{
			marray[lengthmc]=0;
			carray[lengthmc]=0;
			lengthmc=lengthmc+1;
		}
	}
	for (int px=lowestvalue;px<=highestvalue;px=px+1){//loop until all possible values a pixel could be rendered in within the quadralatral can be finished.
		size=0;
		for (i=0;i<4;i=i+1){//loop untill done
			xa=xarray[i];//load values into the recycled xa and ya and xb and yb
			ya=yarray[i];
			if ((i+1)>3){//if at end of the array use begining value to complete shape
				xb=xarray[0];
				yb=yarray[0];
			}
			else{
				xb=xarray[i+1];//otherwise use as normal
				yb=yarray[i+1];
			}
			if (!(xa==xb)){
				if (((xa<px)&&(px<=xb))||((xb<px)&&(px<=xa))){//check in range of line
					out=marray[i]*px+carray[i];
					outs[size]=out;//add to outs
					size=size+1;//size of array of outs that is used
				}
			}
			//simple bubble sort to put the intercepts. in order
			for (int length=0;(length<size-1);length=length+1){
				for (lengthiner=0;(lengthiner<(size-1)-length);lengthiner=lengthiner+1){
					if (outs[lengthiner]>outs[lengthiner+1]){
						temp=outs[lengthiner];
						outs[lengthiner]=outs[lengthiner+1];
						outs[lengthiner+1]=temp;
					}
				}
			}
			//based on the number of intercepts draw a line between them.
			if (size==2){
				//Bdisp_LineC16(px, outs[0], px, outs[1], 1, colour);
				drawlinevert(px, outs[0],outs[1],colour);
			}
			else if (size==4){
				drawlinevert(px, outs[0],outs[1],colour);
				drawlinevert(px, outs[2],outs[3],colour);
			}
			else if (size==3){
				drawlinevert(px, outs[0],outs[2],colour);
			}
		}
	}
	return out;
}
int filledshape(int xa,int ya, int xb, int yb,int xc,int yc,int xd,int yd,unsigned short colour){//shape drawing
	int displaysize=displaywidth-1;//use size of display (384) minus 1 to prevent over shoot if out of range
	int returns=-1;
	//efficency system to avoid scanning lines that are known to be blank
	int greatestbelow=xb;//declare the minimum size for efficiency
	int lowestabove=xa;
	//sorting the values manualy as this turned out to be the quickest as other sorting sytems would have just added complexity.
	if(xa<xb){
		greatestbelow=xa;
		lowestabove=xb;
	}
	if(xc<greatestbelow){
		greatestbelow=xc;
	}
	if(xd<greatestbelow){
		greatestbelow=xd;
	}
	if(greatestbelow<0){
		greatestbelow=0;
	}
	if(xc>lowestabove){
		lowestabove=xc;
	}
	if(xd>lowestabove){
		lowestabove=xd;
	}
	if (lowestabove>displaysize){
		lowestabove=displaysize;
	}
	displaysize=lowestabove;
	returns=drawfilled(xa,ya,xb,yb,xc,yc,xd,yd,colour,greatestbelow,lowestabove);// now pass the values to be rendered as a quadralatral.
	return returns;
}
float sines(int anglein)//sine managements system (legarcy)
{
	float radians = 0;
	float result=0;
	//char disp[12];
	while (anglein>180){// prevent out of bounds for the aproximation
		anglein=anglein-360;
	}
	while (anglein<-180){
		anglein=anglein+360;
	}
    radians=anglein*3.14159/180;//convet to radians
	//use taylor series
    result=radians-((radians*radians*radians)/6)+((radians*radians*radians*radians*radians)/120)-((radians*radians*radians*radians*radians*radians*radians)/5040);
	
	return result;
}
float cosines(int anglein)//cosine management system
{
	float radians = 0;
	float result=0;
	anglein=anglein+90;
	//char disp[12];
	while (anglein>180){// prevent out of bounds for the aproximation
		anglein=anglein-360;
	}
	while (anglein<-180){
		anglein=anglein+360;
	}
    radians=anglein*3.14159/180;//convet to radians
	//use taylor series
	result=radians-((radians*radians*radians)/6)+((radians*radians*radians*radians*radians)/120)-((radians*radians*radians*radians*radians*radians*radians)/5040);
	return result;
}
int intsines(int anglein)//sine managements system this uses integer look up tables.
{
	int lookupval = 0;
	int result=0;
	int arraylookup[91]={0,17,34,52,69,87,104,121,139,156,173,190,207,224,241,258,275,292,309,325,342,358,374,390,406,422,438,453,469,484,499,515,529,544,559,573,587,601,615,629,642,656,669,681,694,707,719,731,743,754,766,777,788,798,809,819,829,838,848,857,866,874,882,891,898,906,913,920,927,933,939,945,951,956,961,965,970,974,978,981,984,987,990,992,994,996,997,998,999,999,1000};
	//char disp[12];
	while (anglein>360){//prevent out of range
		anglein=anglein-360;
	}
	while (anglein<0){
		anglein=anglein+360;
	}
	lookupval=anglein;
	if (lookupval>180){// fold the sine curve so that it can be mapped to the 1/4 of a sine curve 0-90 /360 curve.
		lookupval=360-lookupval;
	}
	if (lookupval>90&&lookupval<=180){
		lookupval=180-lookupval;
	}
	result=arraylookup[lookupval];
	if (anglein>180){
		result=0-result;
	}
    //radians=anglein*(3.14159*100)/180;
    //result=radians-((radians*radians*radians)/60000)+(((radians*radians*radians)/10000)*((radians*radians)/100)/120)-(((radians*radians*radians)/10000)*((radians*radians)/100)*((radians*radians)/100)/5040);
	
	return result;
}
int intcosines(int anglein)//cosine management system
{
	int result;
	result=intsines(anglein+90);// just uses the intsine aproximation. 
	return result;
}
float isqrt(float z) {//this functions is sourced from wikipeidea for more informaiton refreance writeup (small alterations were made for it to work).
	union { float f; long i; } val = {z};	/* Convert type, preserving bit pattern */
	/*
	 * To justify the following code, prove that
	 * 
	 * ((((val.i / 2^m) - b) / 2) + b) * 2^m = ((val.i - 2^m) / 2) + ((b + 1) / 2) * 2^m)
	 *
	 * where
	 *
	 * b = exponent bias
	 * m = number of mantissa bits
	 */
	val.i -= 1 << 23;	/* Subtract 2^m. */
	val.i >>= 1;		/* Divide by 2. */
	val.i += 1 << 29;	/* Add ((b + 1) / 2) * 2^m. */

	return val.f;		/* Interpret again as float */              // c₋₁
}
void perspective(int x, int y, int z, int* rx, int* ry, int* rz )//perspective engine
{
	int resultx=x;
	int resulty=x;
	int zin=z;
	if (z<0){// if behind display pretend it is infront of the display.
		z=0-z;
	}else if (z==0){//if it is zero set to 1 to prevent divide by zero error.
		z=1;
	}
	resultx=(230*((x*1024)/z))/1024;
	resulty=(230*((y*1024)/z))/1024;
	
	*rx=resultx;
	*ry=resulty;
	*rz=zin;
	
}

// these are matrix-like calculations for rotating a shape.
void rotatex(int angle,int x, int y, int z, int* rx, int* ry, int* rz )//rotate x
{
	*rx=x;
	*ry=y*cosines(angle)-(z*sines(angle));
	*rz=y*sines(angle)+z*cosines(angle);
}
void rotatey(int angle,int x, int y, int z, int* rx, int* ry, int* rz )//rotate y
{
	*rx=x*cosines(angle)+z*sines(angle);
	*ry=y;
	*rz=-x*sines(angle)+z*cosines(angle);
}
void rotatez(int angle,int x, int y, int z, int* rx, int* ry, int* rz )//rotate z
{
	*rx=x*cosines(angle)-(y*sines(angle));
	*ry=x*sines(angle)+y*cosines(angle);
	*rz=z;
}

void introtatex(int angle,int x, int y, int z, int* rx, int* ry, int* rz )//rotate x but uses the new faster integer system
{
	*rx=x;
	*ry=(y*intcosines(angle)-(z*intsines(angle)))/1000;
	*rz=(y*intsines(angle)+z*intcosines(angle))/1000;
}
void introtatey(int angle,int x, int y, int z, int* rx, int* ry, int* rz )//rotate y but uses the new faster integer system
{
	*rx=(x*intcosines(angle)+z*intsines(angle))/1000;
	*ry=y;
	*rz=(-x*intsines(angle)+z*intcosines(angle))/1000;
}
void introtatez(int angle,int x, int y, int z, int* rx, int* ry, int* rz )//rotate z but uses the new faster integer system
{
	*rx=(x*intcosines(angle)-(y*intsines(angle)))/1000;
	*ry=(x*intsines(angle)+y*intcosines(angle))/1000;
	*rz=z;
}
void applyofset(int x, int y, int z, int ofsetx, int ofsety, int ofsetz,int* rx, int* ry, int* rz ){//aplys a given ofset to any set of points.
	*rx=x+ofsetx;
	*ry=y+ofsety;
	*rz=z+ofsetz;
}

void cuberender(cube cubein,int rotx,int roty,int rotz,int ofsetx,int ofsety, int ofsetz,unsigned short colour[6],int sideenable[6]){//renders cube provided
	float tempval=0;
	int bcounta=0;
	int bcountb=0;
	int loopholder=0;
	int tempx=0;
	int tempy=0;
	int tempz=0;
	int anglex=rotx;
	int angley=roty;
	int anglez=rotz;
	int averagez[6];
	//the distance aproximaitons.
	float distancetempa=0;
	float distancetempb=0;
	float distancetempc=0;
	float distancetempd=0;
	//int looplen=0;   //not currently used
	unsigned short tempcolour;
	int tempenable;
	quad* pointertop[6];
	quad* tempvalquad;
	cube tempcube;//declare the rotated cubes
	quad sidea;
	quad sideb;
	quad sidec;
	quad sided;
	quad sidee;
	quad sidef;
	//unsigned short newcolour[6];   //not currently used
	tempcube=cubein;
	//process order
	//offsets -> roations (yxz) perspective distortion
	applyofset(cubein.a.x,cubein.a.y,cubein.a.z,ofsetx,ofsety,ofsetz,&tempx,&tempy,&tempz);
	introtatey(angley,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about the y axis
	introtatex(anglex,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotate about the x axis
	introtatez(anglez,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about te z axis
	//perspective(tempx,tempy,tempz,&tempx,&tempy,&tempz);
	tempcube.a.x=tempx;
	tempcube.a.y=tempy;
	tempcube.a.z=tempz;
	applyofset(cubein.b.x,cubein.b.y,cubein.b.z,ofsetx,ofsety,ofsetz,&tempx,&tempy,&tempz);
	introtatey(angley,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about the y axis
	introtatex(anglex,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotate about the x axis
	introtatez(anglez,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about te z axis
	//perspective(tempx,tempy,tempz,&tempx,&tempy,&tempz);
	tempcube.b.x=tempx;
	tempcube.b.y=tempy;
	tempcube.b.z=tempz;
	applyofset(cubein.c.x,cubein.c.y,cubein.c.z,ofsetx,ofsety,ofsetz,&tempx,&tempy,&tempz);
	introtatey(angley,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about the y axis
	introtatex(anglex,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotate about the x axis
	introtatez(anglez,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about te z axis
	//perspective(tempx,tempy,tempz,&tempx,&tempy,&tempz);
	tempcube.c.x=tempx;
	tempcube.c.y=tempy;
	tempcube.c.z=tempz;
	applyofset(cubein.d.x,cubein.d.y,cubein.d.z,ofsetx,ofsety,ofsetz,&tempx,&tempy,&tempz);
	introtatey(angley,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about the y axis
	introtatex(anglex,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotate about the x axis
	introtatez(anglez,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about te z axis
	//perspective(tempx,tempy,tempz,&tempx,&tempy,&tempz);
	tempcube.d.x=tempx;
	tempcube.d.y=tempy;
	tempcube.d.z=tempz;
	applyofset(cubein.e.x,cubein.e.y,cubein.e.z,ofsetx,ofsety,ofsetz,&tempx,&tempy,&tempz);
	introtatey(angley,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about the y axis
	introtatex(anglex,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotate about the x axis
	introtatez(anglez,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about te z axis
	//perspective(tempx,tempy,tempz,&tempx,&tempy,&tempz);
	tempcube.e.x=tempx;
	tempcube.e.y=tempy;
	tempcube.e.z=tempz;
	applyofset(cubein.f.x,cubein.f.y,cubein.f.z,ofsetx,ofsety,ofsetz,&tempx,&tempy,&tempz);
	introtatey(angley,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about the y axis
	introtatex(anglex,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotate about the x axis
	introtatez(anglez,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about te z axis
	//perspective(tempx,tempy,tempz,&tempx,&tempy,&tempz);
	tempcube.f.x=tempx;
	tempcube.f.y=tempy;
	tempcube.f.z=tempz;
	applyofset(cubein.g.x,cubein.g.y,cubein.g.z,ofsetx,ofsety,ofsetz,&tempx,&tempy,&tempz);
	introtatey(angley,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about the y axis
	introtatex(anglex,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotate about the x axis
	introtatez(anglez,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about te z axis
	//perspective(tempx,tempy,tempz,&tempx,&tempy,&tempz);
	tempcube.g.x=tempx;
	tempcube.g.y=tempy;
	tempcube.g.z=tempz;
	applyofset(cubein.h.x,cubein.h.y,cubein.h.z,ofsetx,ofsety,ofsetz,&tempx,&tempy,&tempz);
	introtatey(angley,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about the y axis
	introtatex(anglex,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotate about the x axis
	introtatez(anglez,tempx,tempy,tempz,&tempx,&tempy,&tempz);//rotates about te z axis
	//perspective(tempx,tempy,tempz,&tempx,&tempy,&tempz);
	//sets up the tempcube
	tempcube.h.x=tempx;
	tempcube.h.y=tempy;
	tempcube.h.z=tempz;
	
	sidea.a=tempcube.a;
	sidea.b=tempcube.b;
	sidea.c=tempcube.d;
	sidea.d=tempcube.c;
	
	sideb.a=tempcube.a;
	sideb.b=tempcube.e;
	sideb.c=tempcube.f;
	sideb.d=tempcube.b;
	
	sidec.a=tempcube.a;
	sidec.b=tempcube.e;
	sidec.c=tempcube.g;
	sidec.d=tempcube.c;
	
	sided.a=tempcube.b;
	sided.b=tempcube.d;
	sided.c=tempcube.h;
	sided.d=tempcube.f;
	
	sidee.a=tempcube.c;
	sidee.b=tempcube.d;
	sidee.c=tempcube.h;
	sidee.d=tempcube.g;
	
	sidef.a=tempcube.e;
	sidef.b=tempcube.f;
	sidef.c=tempcube.h;
	sidef.d=tempcube.g;
	
	pointertop[0]=&sidea;//assign pointers to each side to array so this can be sorted easily without actually copying it.
	pointertop[1]=&sideb;
	pointertop[2]=&sidec;
	pointertop[3]=&sided;
	pointertop[4]=&sidee;
	pointertop[5]=&sidef;
	
	for (loopholder=0;loopholder<6;loopholder=loopholder+1){
		/*
		distancetempa=(*pointertop[loopholder]).a.z;
		distancetempb=(*pointertop[loopholder]).b.z;
		distancetempc=(*pointertop[loopholder]).c.z;
		distancetempd=(*pointertop[loopholder]).d.z;
		*/
		//calculat ethe disance each side is away from the display
		distancetempa=isqrt((((*pointertop[loopholder]).a.z)*(*pointertop[loopholder]).a.z)+(((*pointertop[loopholder]).a.x)*(*pointertop[loopholder]).a.x)+(((*pointertop[loopholder]).a.y)*(*pointertop[loopholder]).a.y));
		distancetempb=isqrt((((*pointertop[loopholder]).b.z)*(*pointertop[loopholder]).b.z)+(((*pointertop[loopholder]).b.x)*(*pointertop[loopholder]).b.x)+(((*pointertop[loopholder]).b.y)*(*pointertop[loopholder]).b.y));
		distancetempc=isqrt((((*pointertop[loopholder]).c.z)*(*pointertop[loopholder]).c.z)+(((*pointertop[loopholder]).c.x)*(*pointertop[loopholder]).c.x)+(((*pointertop[loopholder]).c.y)*(*pointertop[loopholder]).c.y));
		distancetempd=isqrt((((*pointertop[loopholder]).d.z)*(*pointertop[loopholder]).d.z)+(((*pointertop[loopholder]).d.x)*(*pointertop[loopholder]).d.x)+(((*pointertop[loopholder]).d.y)*(*pointertop[loopholder]).d.y));
		perspective((*pointertop[loopholder]).a.x,(*pointertop[loopholder]).a.y,(*pointertop[loopholder]).a.z,&((*pointertop[loopholder]).a.x),&((*pointertop[loopholder]).a.y),&((*pointertop[loopholder]).a.z));
		perspective((*pointertop[loopholder]).b.x,(*pointertop[loopholder]).b.y,(*pointertop[loopholder]).b.z,&((*pointertop[loopholder]).b.x),&((*pointertop[loopholder]).b.y),&((*pointertop[loopholder]).b.z));
		perspective((*pointertop[loopholder]).c.x,(*pointertop[loopholder]).c.y,(*pointertop[loopholder]).c.z,&((*pointertop[loopholder]).c.x),&((*pointertop[loopholder]).c.y),&((*pointertop[loopholder]).c.z));
		perspective((*pointertop[loopholder]).d.x,(*pointertop[loopholder]).d.y,(*pointertop[loopholder]).d.z,&((*pointertop[loopholder]).d.x),&((*pointertop[loopholder]).d.y),&((*pointertop[loopholder]).d.z));
		
		averagez[loopholder]=quadaverage(distancetempa,distancetempb,distancetempc,distancetempd);
	}
	// large bubble sort. to swap at the same time the sides and the colours and wether the side is enabled. these are sorted by distance
	for (bcounta=0;(bcounta<5);bcounta=bcounta+1){
		for (bcountb=0;(bcountb<((5-bcounta)));bcountb=bcountb+1){
			if (averagez[bcountb]<averagez[bcountb+1]){
				tempval=averagez[bcountb];
			   
				averagez[bcountb]=averagez[bcountb+1];
				averagez[bcountb+1]=tempval;//swap the average distances
				
				tempvalquad=pointertop[bcountb];
				pointertop[bcountb]=pointertop[bcountb+1];//swap the side pointers
				pointertop[bcountb+1]=tempvalquad;
				
				tempcolour=colour[bcountb];
				colour[bcountb]=colour[bcountb+1];//swap the colours
				colour[bcountb+1]=tempcolour;
				
				tempenable=sideenable[bcountb];
				sideenable[bcountb]=sideenable[bcountb+1];
				sideenable[bcountb+1]=tempenable;//swap the side enables
			}
		}
	}
	for (loopholder=3;loopholder<6;loopholder=loopholder+1){//render the nearest 3 sides from furthes to nearest.
		if (sideenable[loopholder]==1){//if side is enabled draw it
			if (quadaverage((*pointertop[loopholder]).a.z,(*pointertop[loopholder]).b.z,(*pointertop[loopholder]).c.z,(*pointertop[loopholder]).d.z)>0){//if infront of the camera
				filledshape((*pointertop[loopholder]).a.x+halfdisplaywidth,(*pointertop[loopholder]).a.y+halfdisplayheight, (*pointertop[loopholder]).b.x+halfdisplaywidth, (*pointertop[loopholder]).b.y+halfdisplayheight,(*pointertop[loopholder]).c.x+halfdisplaywidth,(*pointertop[loopholder]).c.y+halfdisplayheight,(*pointertop[loopholder]).d.x+halfdisplaywidth,(*pointertop[loopholder]).d.y+halfdisplayheight,colour[loopholder]);//draw the sie of the cube
			}
		}
	}
}

void maprender(objectdata map[5][4][5],int rotx,int roty,int rotz,int ofsetx,int ofsety,int ofsetz,point3d renderofsetorder[],int mapsizex,int mapsizey,int mapsizez,int rendersize){//function to take the map and use this to decide which cubes and in what order to render the cubes change map size here kvl873
	int gridpositionx=-(ofsetx/100);// te players possition in the grid (player cowardinates shold be negative.)
	int gridpositiony=-(ofsety/100);
	int gridpositionz=-(ofsetz/100);
	int gridpositionxremainder=(ofsetx%100);//the remainter of the ofset.
	int gridpositionyremainder=(ofsety%100);
	int gridpositionzremainder=(ofsetz%100);
	int renderloc;
	int sideenables[6];
	objectdata currentobject;
	cube cubet;// a temperary 'cube' is created that is used for the rendering process.
	cubet.a.x=100;
	cubet.a.y=100;
	cubet.a.z=100;
		
	cubet.b.x=0;
	cubet.b.y=100;
	cubet.b.z=100;
	
	cubet.c.x=100;
	cubet.c.y=0;
	cubet.c.z=100;
	
	cubet.d.x=0;
	cubet.d.y=0;
	cubet.d.z=100;
	
	cubet.e.x=100;
	cubet.e.y=100;
	cubet.e.z=0;
	
	cubet.f.x=0;
	cubet.f.y=100;
	cubet.f.z=0;
	
	cubet.g.x=100;
	cubet.g.y=0;
	cubet.g.z=0;
	
	cubet.h.x=0;
	cubet.h.y=0;
	cubet.h.z=0;
	for(renderloc=0;renderloc<(rendersize*rendersize*rendersize);renderloc=renderloc+1){//for the length of renderarray
		if (gridpositionx+renderofsetorder[renderloc].x>=0&&gridpositionx+renderofsetorder[renderloc].x<mapsizex&&gridpositiony+renderofsetorder[renderloc].y>=0&&gridpositiony+renderofsetorder[renderloc].y<mapsizey&&gridpositionz+renderofsetorder[renderloc].z>=0&&gridpositionz+renderofsetorder[renderloc].z<mapsizez){//check if the object being requested is the map
			currentobject =map[gridpositionx+renderofsetorder[renderloc].x][gridpositiony+renderofsetorder[renderloc].y][gridpositionz+renderofsetorder[renderloc].z];// get object
			if (currentobject.nbtdata.invisible==0){//if not invisible
				sideenables[0]=1;
				sideenables[1]=1;
				sideenables[2]=1;
				sideenables[3]=1;
				sideenables[4]=1;
				sideenables[5]=1;
				//disable the sides so that no processing power is wasted on rendering them.
				if (gridpositionx+renderofsetorder[renderloc].x+1>=0&&gridpositionx+renderofsetorder[renderloc].x+1<mapsizex&&gridpositiony+renderofsetorder[renderloc].y>=0&&gridpositiony+renderofsetorder[renderloc].y<mapsizey&&gridpositionz+renderofsetorder[renderloc].z>=0&&gridpositionz+renderofsetorder[renderloc].z<mapsizez){//check a cube inside the array.
					if(map[gridpositionx+renderofsetorder[renderloc].x+1][gridpositiony+renderofsetorder[renderloc].y][gridpositionz+renderofsetorder[renderloc].z].nbtdata.invisible==0){
						sideenables[2]=0;//left
					}
				}
				if (gridpositionx+renderofsetorder[renderloc].x-1>=0&&gridpositionx+renderofsetorder[renderloc].x-1<mapsizex&&gridpositiony+renderofsetorder[renderloc].y>=0&&gridpositiony+renderofsetorder[renderloc].y<mapsizey&&gridpositionz+renderofsetorder[renderloc].z>=0&&gridpositionz+renderofsetorder[renderloc].z<mapsizez){//check a cube inside the array.
					if(map[gridpositionx+renderofsetorder[renderloc].x-1][gridpositiony+renderofsetorder[renderloc].y][gridpositionz+renderofsetorder[renderloc].z].nbtdata.invisible==0){
						sideenables[3]=0;//right
					}
				}
				if (gridpositionx+renderofsetorder[renderloc].x>=0&&gridpositionx+renderofsetorder[renderloc].x<mapsizex&&gridpositiony+renderofsetorder[renderloc].y+1>=0&&gridpositiony+renderofsetorder[renderloc].y+1<mapsizey&&gridpositionz+renderofsetorder[renderloc].z>=0&&gridpositionz+renderofsetorder[renderloc].z<mapsizez){//check a cube inside the array.
					if(map[gridpositionx+renderofsetorder[renderloc].x][gridpositiony+renderofsetorder[renderloc].y+1][gridpositionz+renderofsetorder[renderloc].z].nbtdata.invisible==0){
						sideenables[1]=0;//top
					}
				}
				if (gridpositionx+renderofsetorder[renderloc].x>=0&&gridpositionx+renderofsetorder[renderloc].x<mapsizex&&gridpositiony+renderofsetorder[renderloc].y-1>=0&&gridpositiony+renderofsetorder[renderloc].y-1<mapsizey&&gridpositionz+renderofsetorder[renderloc].z>=0&&gridpositionz+renderofsetorder[renderloc].z<mapsizez){//check a cube inside the array.
					if(map[gridpositionx+renderofsetorder[renderloc].x][gridpositiony+renderofsetorder[renderloc].y-1][gridpositionz+renderofsetorder[renderloc].z].nbtdata.invisible==0){
						sideenables[4]=0;//bottom
					}
				}
				if (gridpositionx+renderofsetorder[renderloc].x>=0&&gridpositionx+renderofsetorder[renderloc].x<mapsizex&&gridpositiony+renderofsetorder[renderloc].y>=0&&gridpositiony+renderofsetorder[renderloc].y<mapsizey&&gridpositionz+renderofsetorder[renderloc].z+1>=0&&gridpositionz+renderofsetorder[renderloc].z+1<mapsizez){
					if(map[gridpositionx+renderofsetorder[renderloc].x][gridpositiony+renderofsetorder[renderloc].y][gridpositionz+renderofsetorder[renderloc].z+1].nbtdata.invisible==0){
						sideenables[0]=0;//back
					}
				}
				if (gridpositionx+renderofsetorder[renderloc].x>=0&&gridpositionx+renderofsetorder[renderloc].x<mapsizex&&gridpositiony+renderofsetorder[renderloc].y>=0&&gridpositiony+renderofsetorder[renderloc].y<mapsizey&&gridpositionz+renderofsetorder[renderloc].z-1>=0&&gridpositionz+renderofsetorder[renderloc].z-1<mapsizez){
					if(map[gridpositionx+renderofsetorder[renderloc].x][gridpositiony+renderofsetorder[renderloc].y][gridpositionz+renderofsetorder[renderloc].z-1].nbtdata.invisible==0){
						sideenables[5]=0;//front
					}
				}
				cuberender(cubet,rotx,roty,rotz,gridpositionxremainder+renderofsetorder[renderloc].x*100,gridpositionyremainder+renderofsetorder[renderloc].y*100,gridpositionzremainder+renderofsetorder[renderloc].z*100,currentobject.colours,sideenables);//render a cube at the location
			}
		}
	}
}
void uidisplay(int anglex,int angley,int anglez,int speed,int ofsetx,int ofsety,int ofsetz,int mode,hotbardata hotbar[5],unsigned short selected){//displays ui elements this is always process after the map rendering.
	//setup vars for printminimini
	char disp[12]="            ";
	int formulax=0;
	int minix=100;
	int miniy=180;
	itoa((speed),(unsigned char*)disp+2);// change variable speed to a array of characters.
	PrintXY(2,5,"       ",0,1);//clear display area
	PrintXY(2,5,disp,0,1);// display the text
	if (mode==0){ //display the differnt modes.
		PrintXY(1,1,"  mode r",0,TEXT_COLOR_RED); 
		itoa((anglex),(unsigned char*)disp+2);//display the rotations
	    PrintXY(2,2,"       ",0,1);
	    PrintXY(2,2,disp,0,1);
	    itoa((angley),(unsigned char*)disp+2);
	    PrintXY(2,3,"       ",0,1);
	    PrintXY(2,3,disp,0,1);
	    itoa((anglez),(unsigned char*)disp+2);
        PrintXY(2,4,"       ",0,1);
	    PrintXY(2,4,disp,0,1);
	}
	else{//motion mode
		PrintXY(1,1,"  mode m",0,TEXT_COLOR_RED);
		itoa((ofsetx),(unsigned char*)disp+2);//display the ofsets
	    PrintXY(2,2,"       ",0,1);
	    PrintXY(2,2,disp,0,1);
	    itoa((ofsety),(unsigned char*)disp+2);
	    PrintXY(2,3,"       ",0,1);
	    PrintXY(2,3,disp,0,1);
	    itoa((ofsetz),(unsigned char*)disp+2);
        PrintXY(2,4,"       ",0,1);
	    PrintXY(2,4,disp,0,1);
	}
	//crosshair
	safepointvram( halfdisplaywidth,halfdisplayheight, 0);
	safepointvram( halfdisplaywidth+1,halfdisplayheight, 0);
	safepointvram( halfdisplaywidth,halfdisplayheight+1, 0);
	safepointvram( halfdisplaywidth-1,halfdisplayheight, 0);
	safepointvram( halfdisplaywidth,halfdisplayheight-1, 0);
	safepointvram( halfdisplaywidth,halfdisplayheight, 0);
	safepointvram( halfdisplaywidth+2,halfdisplayheight, 0);
	safepointvram( halfdisplaywidth,halfdisplayheight+2, 0);
	safepointvram( halfdisplaywidth-2,halfdisplayheight, 0);
	safepointvram( halfdisplaywidth,halfdisplayheight-2, 0);
	//hotbar
	//outline
	drawlinehoriz(190, 128, 260, 0);
	drawlinehoriz(168, 128, 260, 0);
	drawlinevert(128, 169, 190, 0);
	drawlinevert(150, 169, 190, 0);
	drawlinevert(172, 169, 190, 0);
	drawlinevert(194, 169, 190, 0);
	drawlinevert(216, 169, 190, 0);
	drawlinevert(238, 169, 190, 0);
	drawlinevert(260, 169, 190, 0);
	//render the icons
	for (int temp=0;temp<5;temp=temp+1){//for the items int the hotbar
		if(hotbar[temp].quantity>0){
			formulax=129+temp*22;//where the icon is to be placed
			//place quantity of the item
			itoa((hotbar[temp].quantity),(unsigned char*)disp);
			minix=formulax+5;
			for(int iconpointrenderx=0;iconpointrenderx<5;iconpointrenderx=iconpointrenderx+1){
				for(int iconpointrendery=0;iconpointrendery<5;iconpointrendery=iconpointrendery+1){
					largepix(formulax+iconpointrenderx*4,169+iconpointrendery*4,hotbar[temp].icon[iconpointrenderx+iconpointrendery*5]);//draw the icons on the hotbar
				}
			}
			PrintMiniMini(&minix,&miniy, &disp, 0b00000010, 0, 0);//draw the item quantity. the 0b00000010 flag is used to set there to be no background.
		}
	}
	//hilight the slot selected
	formulax=127+selected*22;//formula to work out where to draw the hilight box
	drawlinehoriz(167,formulax,formulax+24,0);
	drawlinehoriz(191,formulax,formulax+24,0);
	drawlinevert(formulax,168,190,0);
	drawlinevert(formulax+24,168,190,0);
}

objectdata checkmapblock(objectdata map[5][4][5],int mapsizex,int mapsizey,int mapsizez,int blockx,int blocky,int blockz){//check block in array. edit the mapsize here (kvl873)
	objectdata returnobject;
	nbt returnobjectnbt;
	returnobjectnbt.type=0;
	returnobjectnbt.breakingpoints=0;
	returnobjectnbt.invisible=1;
	returnobjectnbt.invincible=1;
	returnobjectnbt.placeable=0;
	returnobject.nbtdata=returnobjectnbt;
	if(blockx>=0&&blocky>=0&&blockz>=0){//check if definatly inside of the array prevent truncation issues
		blockx=blockx/100;
		blocky=blocky/100;
		blockz=blockz/100;
		if(blockx>=0&&blockx<mapsizex&&blocky>=0&&blocky<mapsizey&&blockz>=0&&blockz<mapsizez){//check in side of the array
			returnobject=map[blockx][blocky][blockz];
		}
	}
	return returnobject;
} 

void game(int gamemode) {
	//int gamemode=1;// 0=creative 1=survival
	int key;
	char disp[12]="            ";
	//vramstatic =GetVRAMAddress();
	const int arraysize=8;//must be even provides diameter for the render array of distance.
	const int mapsizex=5;//actual map size here is the mapsize kjl78
	const int mapsizey=4;
	const int mapsizez=5;
	int loopvalx=0;
	int loopvaly=0;
	int loopvalz=0;
	int bubblesorta=0;
	int bubblesortb=0;
	int numberofblocktypes=4;
	point3d temppoint3d;
	int temp;
	point3d renderofsetorder[arraysize*arraysize*arraysize];// the use of point 3d means that there are conveniantly 3 values.
	int distanceofsetorder[arraysize*arraysize*arraysize];
	//load array of ofsets to use to render
	int count=0;
	for (loopvalx=0;loopvalx<arraysize;loopvalx=loopvalx+1){//2d loop to work out the distance of everything.
		for (loopvaly=0;loopvaly<arraysize;loopvaly=loopvaly+1){
			for (loopvalz=0;loopvalz<arraysize;loopvalz=loopvalz+1){
				distanceofsetorder[count]=(loopvalx-(arraysize/2))*(loopvalx-((arraysize)/2))+(loopvaly-((arraysize)/2))*(loopvaly-((arraysize)/2))+(loopvalz-((arraysize)/2))*(loopvalz-((arraysize)/2));
				renderofsetorder[count].x=(loopvalx-(arraysize/2));
				renderofsetorder[count].y=(loopvaly-(arraysize/2));
				renderofsetorder[count].z=(loopvalz-(arraysize/2));
				count=count+1;
			}
		}
	}
	for (bubblesorta=0;(bubblesorta<(arraysize*arraysize*arraysize)-1);bubblesorta=bubblesorta+1){//this is a bubblee sort to sort which order to render the cubes in
        for (bubblesortb=0;(bubblesortb<((arraysize*arraysize*arraysize)-1)-bubblesorta);bubblesortb=bubblesortb+1){
            if (distanceofsetorder[bubblesortb]<distanceofsetorder[bubblesortb+1]){
				
                temp=distanceofsetorder[bubblesortb];
                distanceofsetorder[bubblesortb]=distanceofsetorder[bubblesortb+1];
                distanceofsetorder[bubblesortb+1]=temp;//distances swaped.
				
				temppoint3d=renderofsetorder[bubblesortb];
                renderofsetorder[bubblesortb]=renderofsetorder[bubblesortb+1];
                renderofsetorder[bubblesortb+1]=temppoint3d;//swap each of the render ofsets.
            }
        }
    }

	//load cube data aka configure blocks caz4f3 each block needs the nbt data to be setup and the colours to be setup
	nbt cubenbtair;
	cubenbtair.type=0;
	cubenbtair.breakingpoints=0;
	cubenbtair.invisible=1;
	cubenbtair.invincible=1;
	cubenbtair.placeable=0;
	objectdata cubeair;
	cubeair.nbtdata=cubenbtair;
	
	unsigned short stonecolours[6] ={COLOR_DARKSLATEGRAY,COLOR_GRAY,COLOR_GRAY,COLOR_GRAY,COLOR_LIGHTSLATEGRAY,COLOR_GRAY};
	//unsigned short stonecolours[6] ={COLOR_GREEN,COLOR_RED,COLOR_GRAY,COLOR_BLUE,COLOR_LIGHTSLATEGRAY,COLOR_YELLOW};
	nbt cubenbtstone;
	cubenbtstone.type=1;
	cubenbtstone.breakingpoints=5;
	cubenbtstone.invisible=0;
	cubenbtstone.invincible=0;
	cubenbtstone.placeable=1;
	objectdata cubestone;
	cubestone.nbtdata=cubenbtstone;
	cubestone.colours[0]=stonecolours[0];
	cubestone.colours[1]=stonecolours[1];
	cubestone.colours[2]=stonecolours[2];
	cubestone.colours[3]=stonecolours[3];
	cubestone.colours[4]=stonecolours[4];
	cubestone.colours[5]=stonecolours[5];
	//configure block grass
	unsigned short grasscolours[6] ={COLOR_SADDLEBROWN,COLOR_SADDLEBROWN,COLOR_SADDLEBROWN,COLOR_SADDLEBROWN,COLOR_LAWNGREEN,COLOR_SADDLEBROWN};
	nbt cubenbtgrass;
	cubenbtgrass.type=2;
	cubenbtgrass.breakingpoints=2;
	cubenbtgrass.invisible=0;
	cubenbtgrass.invincible=0;
	cubenbtgrass.placeable=1;
	objectdata cubegrass;
	cubegrass.nbtdata=cubenbtgrass;
	cubegrass.colours[0]=grasscolours[0];
	cubegrass.colours[1]=grasscolours[1];
	cubegrass.colours[2]=grasscolours[2];
	cubegrass.colours[3]=grasscolours[3];
	cubegrass.colours[4]=grasscolours[4];
	cubegrass.colours[5]=grasscolours[5];
	//configure block log
	unsigned short logcolours[6] ={COLOR_SADDLEBROWN,COLOR_SANDYBROWN,COLOR_SADDLEBROWN,COLOR_SADDLEBROWN,COLOR_SANDYBROWN,COLOR_SADDLEBROWN};
	//unsigned short stonecolours[6] ={COLOR_GREEN,COLOR_RED,COLOR_GRAY,COLOR_BLUE,COLOR_LIGHTSLATEGRAY,COLOR_YELLOW};
	nbt cubenbtlog;
	cubenbtlog.type=3;
	cubenbtlog.breakingpoints=3;
	cubenbtlog.invisible=0;
	cubenbtlog.invincible=0;
	cubenbtlog.placeable=1;
	objectdata cubelog;
	cubelog.nbtdata=cubenbtlog;
	cubelog.colours[0]=logcolours[0];
	cubelog.colours[1]=logcolours[1];
	cubelog.colours[2]=logcolours[2];
	cubelog.colours[3]=logcolours[3];
	cubelog.colours[4]=logcolours[4];
	cubelog.colours[5]=logcolours[5];
	
	objectdata map[5][4][5]={//this is a 3d array that stores all of the cubes in the map in the form map[x][y][z] (qzo98) (kvl873)(qpz167)
	{{cubestone, cubeair, cubeair, cubestone,cubeair},{cubeair, cubeair, cubeair, cubeair,cubeair}, {cubeair, cubeair, cubeair, cubeair,cubeair}, {cubeair, cubeair, cubeair, cubeair,cubeair}},
    {{cubeair, cubeair, cubeair, cubeair,cubeair},{cubeair, cubeair, cubegrass, cubeair,cubeair}, {cubeair, cubegrass, cubeair, cubeair,cubeair}, {cubeair, cubestone, cubegrass, cubestone,cubeair}},
	{{cubeair, cubeair, cubeair, cubeair,cubeair},{cubeair, cubeair, cubegrass, cubeair,cubeair}, {cubegrass, cubegrass, cubegrass, cubeair,cubeair}, {cubeair, cubestone, cubestone, cubestone,cubegrass}},
	{{cubestone, cubeair, cubeair, cubestone,cubeair},{cubeair, cubeair, cubeair, cubeair,cubeair}, {cubeair, cubeair, cubeair, cubeair,cubeair}, {cubestone, cubeair, cubeair, cubeair,cubeair}},
    {{cubeair, cubeair, cubeair, cubeair,cubeair},{cubeair, cubeair, cubeair, cubeair,cubeair}, {cubeair, cubeair, cubeair, cubeair,cubeair}, {cubeair, cubeair, cubeair, cubeair,cubeair}}};
	int loop=1;
	int mode=0;
	int speed=10;
	int ofsetx=-150;
	int ofsety=0;
	int ofsetz=-150;
	int anglex=0;
	int angley=0;
	int anglez=0;
	//unsigned short pushcolour[6];
	//unsigned short pushclear[6];

	//int breakableflag=0;
	objectdata changeobject;
	
	
	//hotbar data setup
	//also setup the blocklist and add your new block and change the size of the block list to fit. (dzf52)
	short int selectedhotbar =0;
	hotbardata blocklist[4];//3 block types exist air stone and dirt
	hotbardata hotbar[5];
	blocklist[0].object=cubegrass;
	blocklist[0].quantity=0;
	blocklist[0].stacklimit=16;
	//copy the icon data over one by one.
	unsigned short cubegrassicon[25] ={COLOR_GREEN,COLOR_GREEN,COLOR_GREEN,COLOR_GREEN,COLOR_GREEN,COLOR_GREEN,COLOR_SADDLEBROWN,COLOR_GREEN,COLOR_BROWN,COLOR_GREEN,COLOR_BROWN,COLOR_BROWN,COLOR_BROWN,COLOR_BROWN,COLOR_BROWN,COLOR_SADDLEBROWN,COLOR_BROWN,COLOR_BROWN,COLOR_BROWN,COLOR_BROWN,COLOR_BROWN,COLOR_BROWN,COLOR_SADDLEBROWN,COLOR_BROWN,COLOR_BROWN};
	
	for (temp=0;temp<25;temp=temp+1){
		blocklist[0].icon[temp]=cubegrassicon[temp];
	}
	
	blocklist[1].object=cubestone;
	blocklist[1].quantity=0;
	blocklist[1].stacklimit=16;
	unsigned short cubestoneicon[25]={COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_GRAY,COLOR_LIGHTGRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_GRAY,COLOR_GRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_DIMGRAY,COLOR_LIGHTGRAY,COLOR_GRAY,COLOR_DIMGRAY,COLOR_GRAY,COLOR_GRAY,COLOR_GRAY,COLOR_DIMGRAY};
	for (temp=0;temp<25;temp=temp+1){
		blocklist[1].icon[temp]=cubestoneicon[temp];
	}

	
	unsigned short cubeairicon[25]={COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE,COLOR_WHITE};
	blocklist[2].object=cubeair;
	blocklist[2].quantity=0;
	blocklist[2].stacklimit=1;
	for (temp=0;temp<25;temp=temp+1){
		blocklist[2].icon[temp]=cubeairicon[temp];
	}
	
	unsigned short cubelogicon[25]={COLOR_BROWN,COLOR_SADDLEBROWN,COLOR_SADDLEBROWN,COLOR_SANDYBROWN,COLOR_BROWN,COLOR_BROWN,COLOR_SADDLEBROWN,COLOR_BROWN,COLOR_SANDYBROWN,COLOR_BROWN,COLOR_SADDLEBROWN,COLOR_SADDLEBROWN,COLOR_BROWN,COLOR_SADDLEBROWN,COLOR_SADDLEBROWN,COLOR_SANDYBROWN,COLOR_SADDLEBROWN,COLOR_BROWN,COLOR_SADDLEBROWN,COLOR_BROWN,COLOR_SANDYBROWN,COLOR_BROWN,COLOR_SADDLEBROWN,COLOR_SADDLEBROWN,COLOR_BROWN};
	blocklist[3].object=cubelog;
	blocklist[3].quantity=0;
	blocklist[3].stacklimit=16;
	for (temp=0;temp<25;temp=temp+1){
		blocklist[3].icon[temp]=cubelogicon[temp];
	}
	//assigne the blocklist data to the starting hotbar set a hotbar quantity (lk78q)
	hotbar[0]=blocklist[0];
	hotbar[0].quantity=8;
	hotbar[1]=blocklist[1];
	hotbar[1].quantity=8;
	hotbar[2]=blocklist[3];
	hotbar[2].quantity=8;
	hotbar[3]=blocklist[2];
	hotbar[4]=blocklist[2];
	

	while(loop)//frame loop
	{ 
		maprender(map,anglex,angley,anglez,ofsetx,ofsety,ofsetz,renderofsetorder,mapsizex,mapsizey,mapsizez,arraysize);//rendermap/cubes

		//cuberender(cubet,anglex,angley,anglez,ofsetx,ofsety,ofsetz,pushcolour);
		uidisplay( anglex,angley,anglez,speed,ofsetx,ofsety,ofsetz,mode,hotbar,selectedhotbar);//render ui eliments (non map/cube rendering)
		//Bdisp_SetPoint_VRAM(50,50,1);//works to set point see simlo docs https://bible.planet-casio.com/simlo/chm/v20/fxCG20_display.htm
		GetKey(&key);
		//filledshape(30,30, 20, 10,40,90,10,40,COLOR_GREEN);
		//filledshape(200,30, 20, 90,40,90,10,40,COLOR_BLUE);
		//filledshape(10,10, 30, 100,60,10,30,50,COLOR_RED);
		// Blocking is GOOD.  This gets standard keys processed and, possibly, powers down the CPU while waiting
		if (key && key == KEY_CHAR_7){//key 7 swaps the mode
			mode=1-mode;
		}
		if (key && key == KEY_CHAR_9){//key nine swap the speed.
			if (speed==1){
				speed=speed+9;
			}
			else{
				speed=1;
			}
		}
		if (key && key == KEY_CHAR_4){// move player up and down or rotate depending on mode
			if (mode==0){
				anglez=anglez+speed;
			  }
			  else{
				ofsety=ofsety+speed;
			}
		}
		if (key && key == KEY_CHAR_5){// move player up and down or rotate depending on mode
			if (mode==0){
				anglez=anglez-speed;
			}
			else{
				ofsety=ofsety-speed;
			}
		}
		if (key && key == KEY_CTRL_UP){//move forwards or change angle.
			  if (mode==0){
				anglex=anglex-speed;
			}else{//if move forward.
				if (gamemode==0){//if the game mode is creative. there is not need to handle physics.
					ofsetz=(ofsetz*1000-intcosines(0-angley)*speed)/1000;//work out the corrosponding movement based on angle.
					ofsetx=(ofsetx*1000-intsines(0-angley)*speed)/1000;
				}else{
					int gridpositionx=-(ofsetx/100);
					int gridpositiony=-(ofsety/100);
					int gridpositionz=-(ofsetz/100);
					int testlocationz=(ofsetz*1000-intcosines(0-angley)*speed)/1000;
					int testlocationx=(ofsetx*1000-intsines(0-angley)*speed)/1000;
					int testlocationy=ofsety;
					//handle the physics depending on which blocks are there
					if (!(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy-150),-testlocationz).nbtdata.type==0)){
						if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy),-testlocationz).nbtdata.type==0){
							if(checkmapblock(map,mapsizex,mapsizey,mapsizez,gridpositionx*100,(gridpositiony-1)*100,gridpositionz*100).nbtdata.type==0){//check block above player
								if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy+100),-testlocationz).nbtdata.type==0){
									ofsetz=(ofsetz*1000-intcosines(0-angley)*speed)/1000;
									ofsetx=(ofsetx*1000-intsines(0-angley)*speed)/1000;
									ofsety=ofsety+100;
								}
							}
						}
					}else{
						if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-testlocationy,-testlocationz).nbtdata.type==0){
							ofsetz=(ofsetz*1000-intcosines(0-angley)*speed)/1000;
							ofsetx=(ofsetx*1000-intsines(0-angley)*speed)/1000;
						}
					}
					gridpositionx=-(ofsetx/100);
					gridpositionz=-(ofsetz/100);
					//fall as far as possible
					while (checkmapblock(map,mapsizex,mapsizey,mapsizez,gridpositionx*100,-(ofsety-152),gridpositionz*100).nbtdata.type==0&&ofsety>-1000){//fall untill you colide with a block or you reach -1000
						ofsety=ofsety-1;
					}
					
				}
			}
		}
		if (key && key == KEY_CTRL_DOWN){
			if (mode==0){
				anglex=anglex+speed;
			}else{
				if (gamemode==0){
					ofsetz=(ofsetz*1000+intcosines(0-angley)*speed)/1000;//the 0- does nothing in this case but keeps it ocnsistant
					ofsetx=(ofsetx*1000+intsines(0-angley)*speed)/1000;//the angles are in reverse order as the object is rotated not the player so is 0-angley
				}else{
					int gridpositionx=-(ofsetx/100);
					int gridpositiony=-(ofsety/100);
					int gridpositionz=-(ofsetz/100);
					int testlocationz=(ofsetz*1000+intcosines(0-angley)*speed)/1000;
					int testlocationx=(ofsetx*1000+intsines(0-angley)*speed)/1000;
					int testlocationy=ofsety;
					//handle the physics depending on which blocks are there
					if (!(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy-150),-testlocationz).nbtdata.type==0)){
						if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy),-testlocationz).nbtdata.type==0){
							if(checkmapblock(map,mapsizex,mapsizey,mapsizez,gridpositionx*100,(gridpositiony-1)*100,gridpositionz*100).nbtdata.type==0){//check block above player
								if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-((testlocationy+100)),-testlocationz).nbtdata.type==0){
									ofsetz=(ofsetz*1000+intcosines(0-angley)*speed)/1000;
									ofsetx=(ofsetx*1000+intsines(0-angley)*speed)/1000;
									ofsety=ofsety+100;
								}
							}
						}
					}else{
						if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-testlocationy,-testlocationz).nbtdata.type==0){
							ofsetz=(ofsetz*1000+intcosines(0-angley)*speed)/1000;
							ofsetx=(ofsetx*1000+intsines(0-angley)*speed)/1000;
						}
					}
					gridpositionx=-(ofsetx/100);
					gridpositionz=-(ofsetz/100);
					//fall as far as possible
					while (checkmapblock(map,mapsizex,mapsizey,mapsizez,gridpositionx*100,-(ofsety-152),gridpositionz*100).nbtdata.type==0&&ofsety>-1000){//fall untill you colide with a block or you reach -1000
						ofsety=ofsety-1;
					}
				}
			}
		}
		if (key && key == KEY_CTRL_LEFT){
			if (mode==0){
				angley=angley+speed;
			}else{
				if (gamemode==0){//if creative do no checks
					ofsetz=(ofsetz*1000+intcosines((0-angley)+90)*speed)/1000;
					ofsetx=(ofsetx*1000+intsines((0-angley)+90)*speed)/1000;
				}else{
					int gridpositionx=-(ofsetx/100);
					int gridpositiony=-(ofsety/100);
					int gridpositionz=-(ofsetz/100);
					int testlocationz=(ofsetz*1000+intcosines((0-angley)+90)*speed)/1000;
					int testlocationx=(ofsetx*1000+intsines((0-angley)+90)*speed)/1000;
					int testlocationy=ofsety;
					//handle the physics depending on which blocks are there
					if (!(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy-150),-testlocationz).nbtdata.type==0)){
						if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy),-testlocationz).nbtdata.type==0){
							if(checkmapblock(map,mapsizex,mapsizey,mapsizez,gridpositionx*100,(gridpositiony-1)*100,gridpositionz*100).nbtdata.type==0){//check block above player
								if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy+100),-testlocationz).nbtdata.type==0){
									ofsetz=(ofsetz*1000+intcosines((0-angley)+90)*speed)/1000;
									ofsetx=(ofsetx*1000+intsines((0-angley)+90)*speed)/1000;
									ofsety=ofsety+100;
								}
							}
						}
					}else{
						if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-testlocationy,-testlocationz).nbtdata.type==0){
							ofsetz=(ofsetz*1000+intcosines((0-angley)+90)*speed)/1000;
							ofsetx=(ofsetx*1000+intsines((0-angley)+90)*speed)/1000;
						}
					}
					gridpositionx=-(ofsetx/100);
					gridpositionz=-(ofsetz/100);
					//fall as far as possible
					while (checkmapblock(map,mapsizex,mapsizey,mapsizez,gridpositionx*100,-(ofsety-152),gridpositionz*100).nbtdata.type==0&&ofsety>-1000){//fall untill you colide with a block or you reach -1000
						ofsety=ofsety-1;
					}
				}
		    }
		}
		if (key && key == KEY_CTRL_RIGHT){
			if (mode==0){
				angley=angley-speed;
			}else{
				if (gamemode==0){//if creative do no checks
					ofsetz=(ofsetz*1000+intcosines((0-angley)-90)*speed)/1000;
					ofsetx=(ofsetx*1000+intsines((0-angley)-90)*speed)/1000;
				}else{
					int gridpositionx=-(ofsetx/100);
					int gridpositiony=-(ofsety/100);
					int gridpositionz=-(ofsetz/100);
					int testlocationz=(ofsetz*1000+intcosines((0-angley)-90)*speed)/1000;
					int testlocationx=(ofsetx*1000+intsines((0-angley)-90)*speed)/1000;
					int testlocationy=ofsety;
					//handle the physics depending on which blocks are there
					if (!(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-((testlocationy-150)),-testlocationz).nbtdata.type==0)){
						itoa((checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy-150),-testlocationz).nbtdata.type),(unsigned char*)disp+2);
						if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy),-testlocationz).nbtdata.type==0){
							if(checkmapblock(map,mapsizex,mapsizey,mapsizez,gridpositionx*100,(gridpositiony-1)*100,gridpositionz*100).nbtdata.type==0){//check block above player
								if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-(testlocationy+100),-testlocationz).nbtdata.type==0){
									ofsetz=(ofsetz*1000+intcosines((0-angley)-90)*speed)/1000;
									ofsetx=(ofsetx*1000+intsines((0-angley)-90)*speed)/1000;
									ofsety=ofsety+100;
								}
							}
						}
					}else{
						if(checkmapblock(map,mapsizex,mapsizey,mapsizez,-testlocationx,-testlocationy,-testlocationz).nbtdata.type==0){
							ofsetz=(ofsetz*1000+intcosines((0-angley)-90)*speed)/1000;
							ofsetx=(ofsetx*1000+intsines((0-angley)-90)*speed)/1000;
						}
					}
					gridpositionx=-(ofsetx/100);
					gridpositionz=-(ofsetz/100);
					//fall as far as possible
					while (checkmapblock(map,mapsizex,mapsizey,mapsizez,gridpositionx*100,-(ofsety-152),gridpositionz*100).nbtdata.type==0&&ofsety>-1000){//fall untill you colide with a block or you reach -1000
						ofsety=ofsety-1;
					}
				}
		    }
		}
		//hotbar key selection using the functionn keys on the calculator.
		if (key && key == KEY_CTRL_F1){
			selectedhotbar=0;	
		}
		if (key && key == KEY_CTRL_F2){
			selectedhotbar=1;
		}
		if (key && key == KEY_CTRL_F3){
			selectedhotbar=2;
		}
		if (key && key == KEY_CTRL_F4){
			selectedhotbar=3;
		}
		if (key && key == KEY_CTRL_F5){
			selectedhotbar=4;
		}
		if (key && key == KEY_CTRL_DEL){//remove the item from the hotbar.
			if (hotbar[selectedhotbar].quantity>0){//only if there is 1 or more blocks.
				hotbar[selectedhotbar].quantity=hotbar[selectedhotbar].quantity-1;
			}
		}
		if (key && key == KEY_CHAR_MINUS){//- key remove block
			int gridpositionx=-(ofsetx/100);
			int gridpositiony=-(ofsety/100);
			int gridpositionz=-(ofsetz/100);
			int testposx=0;
			int testposy=0;
			int testposz=0;
			int breakableflag=0;
			for (int i=0;i<=400&&breakableflag==0;i=i+1){//gets the grid postions
				testposz=ofsetz-cosines(0-anglex)*cosines(0-angley)*i;
				testposx=ofsetx-cosines(0-anglex)*sines(0-angley)*i;
				testposy=ofsety+sines(0-anglex)*i;
				gridpositionx=-(testposx/100);
				gridpositiony=-(testposy/100);
				gridpositionz=-(testposz/100);
				if (gridpositionx<mapsizex&&gridpositiony<mapsizey&&gridpositionz<mapsizez&&gridpositionx>=0&&gridpositiony>=0&&gridpositionz>=0){
				//if (gridpositionx<=mapsizex&&gridpositiony<=mapsizey&&gridpositionz<=mapsizez&&gridpositionx>=0&&gridpositiony>=0&&gridpositionz>=0){
					changeobject=map[gridpositionx][gridpositiony][gridpositionz];
					if (changeobject.nbtdata.invisible==0){//if it is visible then try to break block and exit loop
						breakableflag=1;
					}
				}
			}
			if (changeobject.nbtdata.invincible==0&&breakableflag==1){
				map[gridpositionx][gridpositiony][gridpositionz].nbtdata.breakingpoints=map[gridpositionx][gridpositiony][gridpositionz].nbtdata.breakingpoints-1;
				if (map[gridpositionx][gridpositiony][gridpositionz].nbtdata.breakingpoints<=0){
					if (gamemode==1){//if survival then increase the number of the held item.
						short int founditemplace=0;
						for (int hotbarvalue=0;hotbarvalue<5&&founditemplace==0;hotbarvalue=hotbarvalue+1){//check if item can go in the hotbar
							if(hotbar[hotbarvalue].object.nbtdata.type==map[gridpositionx][gridpositiony][gridpositionz].nbtdata.type&&hotbar[hotbarvalue].quantity>0&&hotbar[hotbarvalue].quantity<hotbar[hotbarvalue].stacklimit){//if the types match and within stack limits
								hotbar[hotbarvalue].quantity=hotbar[hotbarvalue].quantity+1;//if item already exists in inventory
								founditemplace=1; //make the loop stop
								
							}
						}
						for (int hotbarvalue=0;hotbarvalue<5&&founditemplace==0;hotbarvalue=hotbarvalue+1){//check if item can go in the hotbar
							if(hotbar[hotbarvalue].quantity==0){//if no item of value there 
								for (int findblockindex=0;findblockindex<numberofblocktypes;findblockindex=findblockindex+1){//scan through the block types array for a compatable block profile
									if(blocklist[findblockindex].object.nbtdata.type==map[gridpositionx][gridpositiony][gridpositionz].nbtdata.type){
										founditemplace=1; //make the loop stop
										hotbar[hotbarvalue]=blocklist[findblockindex];
										hotbar[hotbarvalue].quantity=1;
									}
								}
							}
						}
						map[gridpositionx][gridpositiony][gridpositionz]=cubeair;
					}else{
						map[gridpositionx][gridpositiony][gridpositionz]=cubeair;
					}
				}
				if(gamemode==1){//only fall in survival mode
					while (checkmapblock(map,mapsizex,mapsizey,mapsizez,-ofsetx,-(ofsety-152),-ofsetz).nbtdata.type==0&&ofsety>-1000){//fall untill you colide with a block or you reach -1000
						ofsety=ofsety-1;
					}
				}
			}
		}
		if (key && key == KEY_CHAR_PLUS){//+ key add block
			int gridpositionx=-(ofsetx/100);
			int gridpositiony=-(ofsety/100);
			int gridpositionz=-(ofsetz/100);
			int testposx=0;
			int testposy=0;
			int testposz=0;
			int breakableflag=0;
			for (int i=0;i<=400&&breakableflag==0;i=i+1){
				testposz=ofsetz-cosines(0-anglex)*cosines(0-angley)*i;
				testposx=ofsetx-cosines(0-anglex)*sines(0-angley)*i;
				testposy=ofsety+sines(0-anglex)*i;
				gridpositionx=-(testposx/100);//gets the grid postions
				gridpositiony=-(testposy/100);
				gridpositionz=-(testposz/100);
				if (gridpositionx<mapsizex&&gridpositiony<mapsizey&&gridpositionz<mapsizez&&gridpositionx>=0&&gridpositiony>=0&&gridpositionz>=0){
					changeobject=map[gridpositionx][gridpositiony][gridpositionz];
					if (changeobject.nbtdata.invisible==0){//if it is a visible block then it is rendered then you can place against it
						breakableflag=1;
						testposz=ofsetz-cosines(0-anglex)*cosines(0-angley)*(i-1);
						testposx=ofsetx-cosines(0-anglex)*sines(0-angley)*(i-1);
						testposy=ofsety+sines(0-anglex)*(i-1);
						gridpositionx=-(testposx/100);
						gridpositiony=-(testposy/100);
						gridpositionz=-(testposz/100);
					}
				}
			}
			if (breakableflag==1&&gridpositionx<mapsizex&&gridpositiony<mapsizey&&gridpositionz<mapsizez&&gridpositionx>=0&&gridpositiony>=0&&gridpositionz>=0){
				if(gridpositionx!=-(ofsetx/100)||gridpositiony!=-(ofsety/100)||gridpositionz!=-(ofsetz/100)){
					//map[gridpositionx][gridpositiony][gridpositionz]=cubestone;
					if (hotbar[selectedhotbar].object.nbtdata.placeable==1&&hotbar[selectedhotbar].quantity>0){
						map[gridpositionx][gridpositiony][gridpositionz]=hotbar[selectedhotbar].object;
						if (gamemode==1){
							hotbar[selectedhotbar].quantity=hotbar[selectedhotbar].quantity-1;
						}
					}
				}
			}
		}
		if (key && key == KEY_CTRL_EXIT){
			loop=0;
		}
		Bdisp_AllClr_VRAM();
		//Bdisp_LineC16(1, 1, 100, 100, 1, COLOR_GREEN);
		//Bdisp_PutDisp_DD();
	}
	return;
}
void button(int xa,int ya,int xb,int yb,int charx,int chary,char buttonname[],short int unselectedcolour,short int selectedcolour,int textcolour,short int selected){
	if(selected){//check if the button should display as selected
		for(int i=xa;i<xb;i=i+1){//button selected
			drawlinevert(i,ya,yb,selectedcolour);
		}
		drawlinevert(xa,ya,yb,0);
		drawlinevert(xb,ya,yb,0);
		drawlinehoriz(ya,xa,xb,0);
		drawlinehoriz(yb,xa,xb,0);
	}else{
		for(int i=xa;i<xb;i=i+1){//button unselected
			drawlinevert(i,ya,yb,unselectedcolour);
		}
	}
	PrintXY(charx,chary,buttonname,0x20,textcolour);
}

void menubackground(int seed){
	sys_srand(seed);//set the seed.
	for(int xdraw=0;xdraw<(displaywidth/5);xdraw=xdraw+1){//loop through the display and draw large pixels.
		for(int ydraw=0;ydraw<(displayheight/5);ydraw=ydraw+1){
			seed=sys_rand();//generate a new random number
			if(seed%3){
				largepix(xdraw*5,ydraw*5,COLOR_SADDLEBROWN);//most likely
			}else if(seed%2==0){
				largepix(xdraw*5,ydraw*5,COLOR_BROWN);//equaly least likely
			}else{
				largepix(xdraw*5,ydraw*5,COLOR_GRAY);//equaly least likely
			}
		}
	}
	
}
int main(){
	int key;
	int selectedbutton=0;
	Bdisp_EnableColor(1);
	int loop=1;
	char buttonnameplay[]="  Play";
	char buttonnameexit[]="  Exit";
	char buttonnameoptions[]="  Options";
	int gamemode=1;
	int optionloop=0;
	while (loop){
		//Bdisp_AllClr_VRAM();
		menubackground(5);
		PrintXY(6,1,"  CALCU-CRAFT",0,1);
		if(selectedbutton==0){//render the correct selected buttons
			button(halfdisplaywidth-50,halfdisplayheight-80,halfdisplaywidth+50,halfdisplayheight-30,10,3,buttonnameplay,COLOR_GRAY,COLOR_LIGHTGRAY,0,1);
			button(halfdisplaywidth-75,halfdisplayheight-25,halfdisplaywidth+75,halfdisplayheight+25,8,5,buttonnameoptions,COLOR_GRAY,COLOR_LIGHTGRAY,0,0);
			button(halfdisplaywidth-50,halfdisplayheight+30,halfdisplaywidth+50,halfdisplayheight+80,10,7,buttonnameexit,COLOR_GRAY,COLOR_LIGHTGRAY,0,0);
		}else if(selectedbutton==1){
			button(halfdisplaywidth-50,halfdisplayheight-80,halfdisplaywidth+50,halfdisplayheight-30,10,3,buttonnameplay,COLOR_GRAY,COLOR_LIGHTGRAY,0,0);
			button(halfdisplaywidth-75,halfdisplayheight-25,halfdisplaywidth+75,halfdisplayheight+25,8,5,buttonnameoptions,COLOR_GRAY,COLOR_LIGHTGRAY,0,1);
			button(halfdisplaywidth-50,halfdisplayheight+30,halfdisplaywidth+50,halfdisplayheight+80,10,7,buttonnameexit,COLOR_GRAY,COLOR_LIGHTGRAY,0,0);
		}else{
			button(halfdisplaywidth-50,halfdisplayheight-80,halfdisplaywidth+50,halfdisplayheight-30,10,3,buttonnameplay,COLOR_GRAY,COLOR_LIGHTGRAY,0,0);
			button(halfdisplaywidth-75,halfdisplayheight-25,halfdisplaywidth+75,halfdisplayheight+25,8,5,buttonnameoptions,COLOR_GRAY,COLOR_LIGHTGRAY,0,0);
			button(halfdisplaywidth-50,halfdisplayheight+30,halfdisplaywidth+50,halfdisplayheight+80,10,7,buttonnameexit,COLOR_GRAY,COLOR_LIGHTGRAY,0,1);
		}
		
		GetKey(&key);
		if (key == KEY_CTRL_UP&&selectedbutton>0){
			selectedbutton=selectedbutton-1;
		}
		if(key == KEY_CTRL_DOWN&&selectedbutton<2){
			selectedbutton=selectedbutton+1;
		}
		if (key && key == KEY_CTRL_EXE){
			if(selectedbutton==2){//provide exiting instructions
				PrintXY(1,1,"  Press menu to exit",0,1);
				GetKey(&key);
			}
			if(selectedbutton==1){//enter the options menu
				Bdisp_AllClr_VRAM();
				PrintXY(1,1,"  Options",0,1);
				optionloop=1;
				int selectedoptionmenu=0;
				char survival[]="  Gamemode Survival";
				char creative[]="  Gamemode Creative";
				while(optionloop){
					Bdisp_AllClr_VRAM();
					menubackground(3);
					PrintXY(8,1,"  Options ",0,1);
					//PrintXY(1,2,"  gamemode",0,1);
					if (gamemode==1){//depending on gamemode selected show the selected game mode in the button.
						button(halfdisplaywidth-175,halfdisplayheight-25,halfdisplaywidth+175,halfdisplayheight+25,3,5,survival,COLOR_GRAY,COLOR_LIGHTGRAY,0,1-selectedoptionmenu);
					}else{
						button(halfdisplaywidth-175,halfdisplayheight-25,halfdisplaywidth+175,halfdisplayheight+25,3,5,creative,COLOR_GRAY,COLOR_LIGHTGRAY,0,1-selectedoptionmenu);
					}
					button(halfdisplaywidth-50,halfdisplayheight+30,halfdisplaywidth+50,halfdisplayheight+80,10,7,buttonnameexit,COLOR_GRAY,COLOR_LIGHTGRAY,0,selectedoptionmenu);
					GetKey(&key);
					if(key==KEY_CTRL_DOWN||key==KEY_CTRL_UP){//switch seleted item
						selectedoptionmenu=1-selectedoptionmenu;
					}
					if(key==KEY_CTRL_EXE&&selectedoptionmenu==0){//change the gamemode when exepressed
						gamemode=1-gamemode;
					}if(key==KEY_CTRL_EXE&&selectedoptionmenu){//exit
						optionloop=0;
					}
					if(key==KEY_CTRL_EXIT){//exit
						optionloop=0;
					}
				}
			}
			if(selectedbutton==0){//if play button selected
				Bdisp_AllClr_VRAM();
				game(gamemode);
			}
		}
	}
}